﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IksOks
{
    public partial class Form1 : Form
    {
        Graphics[] g = new Graphics[9];
        Pen pen = new Pen(Color.Black, 3);
        int a, b, n = 10;
        int[] arr  = new int[9];
        string igracx, igraco;
        int xwins, owins;
        public Form1()
        {
            for (int i = 0; i < 9; i++)
                arr [i] = i;
            InitializeComponent();
            g[0] = pB1.CreateGraphics();
            g[1] = pB2.CreateGraphics();
            g[2] = pB3.CreateGraphics();
            g[3] = pB4.CreateGraphics();
            g[4] = pB5.CreateGraphics();
            g[5] = pB6.CreateGraphics();
            g[6] = pB7.CreateGraphics();
            g[7] = pB8.CreateGraphics();
            g[8] = pB9.CreateGraphics();
            a = pB1.Height - 1;
            b = pB1.Width - 1;
        }
        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            
            if (tBPlayer1.Text.ToString() == "" || tBPlayer2.Text.ToString() == "")
                MessageBox.Show("Molimo unesite imena oba igrača", "Greška");
            else {
                if (buttonNewGame.Text == "Započni igru")
                {
                    igracx = tBPlayer1.Text.ToString();
                    igraco = tBPlayer2.Text.ToString();
                    labelPlayerScore1.Text = igracx + ": 0";
                    labelPlayerScore2.Text = igraco + ": 0";
                    tBPlayer1.Hide();
                    tBPlayer2.Hide();
                    label1.Hide();
                    label2.Hide();
                    label3.Show();
                    label4.Show();
                    labelPlayerScore1.Show();
                    labelPlayerScore2.Show();
                    buttonNewGame.Text = "Nova igra";
                    xwins = 0;
                    owins = 0;
                    NewRound();
                }
                else
                {
                    NewRound();
                    n = 10;
                    tBPlayer1.Text = "";
                    tBPlayer2.Text = "";
                    label3.Hide();
                    label4.Hide();
                    labelPlayerScore1.Hide();
                    labelPlayerScore2.Hide();
                    tBPlayer1.Show();
                    tBPlayer2.Show();
                    label1.Show();
                    label2.Show();
                    buttonNewGame.Text = "Započni igru";
                }
            }
        }
        private void pB1_Click(object sender, EventArgs e)
        {
            Odigrajpotez(0);
        }
        private void pB2_Click(object sender, EventArgs e)
        {
            Odigrajpotez(1);
        }
        private void pB3_Click(object sender, EventArgs e)
        {
            Odigrajpotez(2);
        }
        private void pB4_Click(object sender, EventArgs e)
        {
            Odigrajpotez(3);
        }
        private void pB5_Click(object sender, EventArgs e)
        {
            Odigrajpotez(4);
        }
        private void pB6_Click(object sender, EventArgs e)
        {
            Odigrajpotez(5);
        }
        private void pB7_Click(object sender, EventArgs e)
        {
            Odigrajpotez(6);
        }
        private void pB8_Click(object sender, EventArgs e)
        {
            Odigrajpotez(7);
        }
        private void pB9_Click(object sender, EventArgs e)
        {
            Odigrajpotez(8);
        }
        public bool isWin() {
            if (arr[0] == arr[1] && arr[0] == arr[2] || arr[3] == arr[4] && arr[3] == arr[5] || arr[6] == arr[7] && arr[6] == arr[8])
                return true;
            else if (arr[0] == arr[3] && arr[0] == arr[6] || arr[1] == arr[4] && arr[1] == arr[7] || arr[2] == arr[5] && arr[2] == arr[8])
                return true;
            else if (arr[0] == arr[4] && arr[0] == arr[8] || arr[2] == arr[4] && arr[2] == arr[6])
                return true;
            else
                return false;
        }
        public void Xwin() {
            MessageBox.Show(igracx + " je pobjedio!", "Pobjeda");
            NewRound();
            xwins++;
            labelPlayerScore1.Text = igracx + ": " + xwins.ToString();
        }
        public void Owin() {
            MessageBox.Show(igraco + " je pobjedio!", "Pobjeda");
            NewRound();
            owins++;
            labelPlayerScore2.Text = igraco + ": " + owins.ToString();
        }
        public void Tied(){
            if (!isWin() && n == 10)
            {
                MessageBox.Show("Nerješeno!", "Nerješeno");
                NewRound();
            }
        }
        public void NewRound() {
            label4.Text = igracx;
            n = 1;
            for (int i = 0; i < 9; i++)
            {
                arr[i] = i;
                g[i].Clear(Color.White);
            }
        }
        public void Odigrajpotez(int i) {
            if (n < 10 && (arr[i] < 10))
            {
                if (n % 2 == 1)
                {
                    g[i].DrawLine(pen, 0, 0, a, b);
                    g[i].DrawLine(pen, a, 0, 0, b);
                    arr[i] = 'X';
                    n++;
                    if (isWin())
                        Xwin();
                    else
                        label4.Text = igraco;
                }
                else
                {
                    g[i].DrawEllipse(pen, 0, 0, a, b);
                    arr[i] = 'O';
                    n++;
                    if (isWin())
                        Owin();
                    else
                        label4.Text = igracx;
                }
                Tied();
            }
        }
    }
}
