﻿namespace IksOks
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pB1 = new System.Windows.Forms.PictureBox();
            this.pB2 = new System.Windows.Forms.PictureBox();
            this.pB3 = new System.Windows.Forms.PictureBox();
            this.pB6 = new System.Windows.Forms.PictureBox();
            this.pB5 = new System.Windows.Forms.PictureBox();
            this.pB4 = new System.Windows.Forms.PictureBox();
            this.pB9 = new System.Windows.Forms.PictureBox();
            this.pB8 = new System.Windows.Forms.PictureBox();
            this.pB7 = new System.Windows.Forms.PictureBox();
            this.buttonNewGame = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tBPlayer1 = new System.Windows.Forms.TextBox();
            this.tBPlayer2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelPlayerScore1 = new System.Windows.Forms.Label();
            this.labelPlayerScore2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).BeginInit();
            this.SuspendLayout();
            // 
            // pB1
            // 
            this.pB1.BackColor = System.Drawing.Color.White;
            this.pB1.Location = new System.Drawing.Point(11, 74);
            this.pB1.Name = "pB1";
            this.pB1.Size = new System.Drawing.Size(100, 100);
            this.pB1.TabIndex = 0;
            this.pB1.TabStop = false;
            this.pB1.Click += new System.EventHandler(this.pB1_Click);
            // 
            // pB2
            // 
            this.pB2.BackColor = System.Drawing.Color.White;
            this.pB2.Location = new System.Drawing.Point(117, 74);
            this.pB2.Name = "pB2";
            this.pB2.Size = new System.Drawing.Size(100, 100);
            this.pB2.TabIndex = 1;
            this.pB2.TabStop = false;
            this.pB2.Click += new System.EventHandler(this.pB2_Click);
            // 
            // pB3
            // 
            this.pB3.BackColor = System.Drawing.Color.White;
            this.pB3.Location = new System.Drawing.Point(223, 74);
            this.pB3.Name = "pB3";
            this.pB3.Size = new System.Drawing.Size(100, 100);
            this.pB3.TabIndex = 2;
            this.pB3.TabStop = false;
            this.pB3.Click += new System.EventHandler(this.pB3_Click);
            // 
            // pB6
            // 
            this.pB6.BackColor = System.Drawing.Color.White;
            this.pB6.Location = new System.Drawing.Point(223, 180);
            this.pB6.Name = "pB6";
            this.pB6.Size = new System.Drawing.Size(100, 100);
            this.pB6.TabIndex = 5;
            this.pB6.TabStop = false;
            this.pB6.Click += new System.EventHandler(this.pB6_Click);
            // 
            // pB5
            // 
            this.pB5.BackColor = System.Drawing.Color.White;
            this.pB5.Location = new System.Drawing.Point(117, 180);
            this.pB5.Name = "pB5";
            this.pB5.Size = new System.Drawing.Size(100, 100);
            this.pB5.TabIndex = 4;
            this.pB5.TabStop = false;
            this.pB5.Click += new System.EventHandler(this.pB5_Click);
            // 
            // pB4
            // 
            this.pB4.BackColor = System.Drawing.Color.White;
            this.pB4.Location = new System.Drawing.Point(11, 180);
            this.pB4.Name = "pB4";
            this.pB4.Size = new System.Drawing.Size(100, 100);
            this.pB4.TabIndex = 3;
            this.pB4.TabStop = false;
            this.pB4.Click += new System.EventHandler(this.pB4_Click);
            // 
            // pB9
            // 
            this.pB9.BackColor = System.Drawing.Color.White;
            this.pB9.Location = new System.Drawing.Point(223, 287);
            this.pB9.Name = "pB9";
            this.pB9.Size = new System.Drawing.Size(100, 100);
            this.pB9.TabIndex = 8;
            this.pB9.TabStop = false;
            this.pB9.Click += new System.EventHandler(this.pB9_Click);
            // 
            // pB8
            // 
            this.pB8.BackColor = System.Drawing.Color.White;
            this.pB8.Location = new System.Drawing.Point(117, 287);
            this.pB8.Name = "pB8";
            this.pB8.Size = new System.Drawing.Size(100, 100);
            this.pB8.TabIndex = 7;
            this.pB8.TabStop = false;
            this.pB8.Click += new System.EventHandler(this.pB8_Click);
            // 
            // pB7
            // 
            this.pB7.BackColor = System.Drawing.Color.White;
            this.pB7.Location = new System.Drawing.Point(11, 287);
            this.pB7.Name = "pB7";
            this.pB7.Size = new System.Drawing.Size(100, 100);
            this.pB7.TabIndex = 6;
            this.pB7.TabStop = false;
            this.pB7.Click += new System.EventHandler(this.pB7_Click);
            // 
            // buttonNewGame
            // 
            this.buttonNewGame.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonNewGame.Location = new System.Drawing.Point(223, 13);
            this.buttonNewGame.Name = "buttonNewGame";
            this.buttonNewGame.Size = new System.Drawing.Size(100, 46);
            this.buttonNewGame.TabIndex = 9;
            this.buttonNewGame.Text = "Započni igru";
            this.buttonNewGame.UseVisualStyleBackColor = false;
            this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Location = new System.Drawing.Point(11, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(312, 313);
            this.panel1.TabIndex = 11;
            // 
            // tBPlayer1
            // 
            this.tBPlayer1.Location = new System.Drawing.Point(117, 13);
            this.tBPlayer1.MaxLength = 12;
            this.tBPlayer1.Name = "tBPlayer1";
            this.tBPlayer1.Size = new System.Drawing.Size(100, 20);
            this.tBPlayer1.TabIndex = 12;
            this.tBPlayer1.Text = "Igrac1";
            // 
            // tBPlayer2
            // 
            this.tBPlayer2.Location = new System.Drawing.Point(117, 39);
            this.tBPlayer2.MaxLength = 12;
            this.tBPlayer2.Name = "tBPlayer2";
            this.tBPlayer2.Size = new System.Drawing.Size(100, 20);
            this.tBPlayer2.TabIndex = 13;
            this.tBPlayer2.Text = "Igrac2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Ime prvog igrača:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Ime drugog igrača:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Na potezu je :";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(106, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Igrac";
            this.label4.Visible = false;
            // 
            // labelPlayerScore1
            // 
            this.labelPlayerScore1.AutoSize = true;
            this.labelPlayerScore1.Location = new System.Drawing.Point(12, 39);
            this.labelPlayerScore1.Name = "labelPlayerScore1";
            this.labelPlayerScore1.Size = new System.Drawing.Size(40, 13);
            this.labelPlayerScore1.TabIndex = 18;
            this.labelPlayerScore1.Text = "Igrac1:";
            this.labelPlayerScore1.Visible = false;
            // 
            // labelPlayerScore2
            // 
            this.labelPlayerScore2.AutoSize = true;
            this.labelPlayerScore2.Location = new System.Drawing.Point(118, 39);
            this.labelPlayerScore2.Name = "labelPlayerScore2";
            this.labelPlayerScore2.Size = new System.Drawing.Size(40, 13);
            this.labelPlayerScore2.TabIndex = 19;
            this.labelPlayerScore2.Text = "Igrac2:";
            this.labelPlayerScore2.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(335, 399);
            this.Controls.Add(this.labelPlayerScore2);
            this.Controls.Add(this.labelPlayerScore1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tBPlayer2);
            this.Controls.Add(this.tBPlayer1);
            this.Controls.Add(this.buttonNewGame);
            this.Controls.Add(this.pB9);
            this.Controls.Add(this.pB8);
            this.Controls.Add(this.pB7);
            this.Controls.Add(this.pB6);
            this.Controls.Add(this.pB5);
            this.Controls.Add(this.pB4);
            this.Controls.Add(this.pB3);
            this.Controls.Add(this.pB2);
            this.Controls.Add(this.pB1);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Iks Oks";
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pB1;
        private System.Windows.Forms.PictureBox pB2;
        private System.Windows.Forms.PictureBox pB3;
        private System.Windows.Forms.PictureBox pB6;
        private System.Windows.Forms.PictureBox pB5;
        private System.Windows.Forms.PictureBox pB4;
        private System.Windows.Forms.PictureBox pB9;
        private System.Windows.Forms.PictureBox pB8;
        private System.Windows.Forms.PictureBox pB7;
        private System.Windows.Forms.Button buttonNewGame;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tBPlayer1;
        private System.Windows.Forms.TextBox tBPlayer2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelPlayerScore1;
        private System.Windows.Forms.Label labelPlayerScore2;
    }
}

